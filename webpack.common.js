const path = require('path')

module.exports = {
  entry: './src/index.js',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: {
          presets: [ '@babel/env' ],
        },
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ],
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif|webp)$/i,
        type: 'asset/resource',
      }
    ],
  },
  resolve: {
    extensions: [ '*', '.js', '.jsx' ],
    alias: {
      utils: path.resolve(path.resolve(), './src/utils'),
      images: path.resolve(path.resolve(), './src/assets/images'),
    },
  },
  output: {
    path: path.resolve(path.resolve(), 'dist/'),
    publicPath: '/dist/',
    filename: 'bundle.js',
  },
  devtool: 'source-map',
}
