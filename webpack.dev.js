const path = require('path')
const webpack = require('webpack')
const { merge } = require('webpack-merge')
const DotenvWebpack = require('dotenv-webpack')
const commonConfig = require('./webpack.common')

module.exports = merge(
  commonConfig,
  {
    mode: 'development',
    devServer: {
      contentBase: path.join(path.resolve(), 'public/'),
      port: 3001,
      publicPath: 'http://localhost:3001/dist/',
      hotOnly: true,
      historyApiFallback: true,
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new DotenvWebpack({
        path: './env/dev.env',
        allowEmptyValues: true,
      }),
    ],
  },
)
