import firebase from 'firebase/app'
import 'firebase/auth'
import config from '../config/index.js'

function app() {
  if (!firebase.apps.length) {
    firebase.initializeApp(config.firebase)
  }

  return firebase.app()
}

function auth() {
  return firebase.auth(app())
}

export default {
  app,
  auth,
}
