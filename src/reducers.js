import { combineReducers } from 'redux'
import authReducer from './features/login/authReducer'

export default combineReducers({
  auth: authReducer,
})
