import { applyMiddleware, createStore } from '@reduxjs/toolkit'
import reducer from './reducers'

const asyncThunkMiddleware = ({ dispatch, getState }) => next => action => {
  if (typeof action === 'function') {
    return action(dispatch, getState)
  }

  return next(action)
}

const store = createStore(reducer, applyMiddleware(asyncThunkMiddleware))

export default store
