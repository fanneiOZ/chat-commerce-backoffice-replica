import React from 'react'
import { BrowserRouter, Switch } from 'react-router-dom'
import Route from './components/Route'
import { Greeting, Login } from './features'
import { PageNotFound } from './components'

export default function App({}) {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/login" children={Login}/>
        <Route path="/greeting" requireLogin children={Greeting}/>
        <Route exact path="/" children={Login}/>
        <Route path="/" children={PageNotFound}/>
      </Switch>
    </BrowserRouter>
  )
}
