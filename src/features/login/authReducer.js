import { createAction, createReducer, createSlice } from '@reduxjs/toolkit'

export const onLoginRequested = createAction('login/onLoginRequested')
export const onLoginFailed = createAction('login/onLoginFailed', error => ({ payload: { error } }))
export const onLoggedIn = createAction('login/onLoggedIn', uid => ({ payload: { uid } }))
export const onLoggedOut = createAction('login/onLoggedOut')

const initialState = {
  uid: undefined,
  error: undefined,
}

function stateBuilder(builder) {
  builder.addCase(onLoggedIn, (state, action) => {
      state.error = undefined
      state.uid = action.payload?.uid
    })
    .addCase(onLoggedOut, (state) => {
      state.error = undefined
      state.uid = undefined
    })
    .addCase(onLoginFailed, (state, action) => {
      state.error = action.payload.error
    })
}

const { reducer } = createSlice({
  name: 'auth',
  initialState,
  reducers: createReducer(initialState, stateBuilder),
})

export default reducer
