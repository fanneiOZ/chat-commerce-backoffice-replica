import React from 'react'
import LoginForm from './components/LoginForm'
import ErrorMessage from './components/ErrorMessage'
import { AppCover } from '../../components'

export default function Login({}) {
  return (
    <>
      <div>Login Page</div>
      <AppCover />
      <ErrorMessage/>
      <LoginForm/>
    </>
  )
}
