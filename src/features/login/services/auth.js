import FirebaseProxy from '../../../utils/firebase.js'
import firebase from 'firebase/app'
import 'firebase/auth'
import store from '../../../store'
import { UserEvents } from '../authReducer'

const provider = new firebase.auth.SAMLAuthProvider('saml.cpall')

/**
 * @returns {Promise<firebase.auth.UserCredential>}
 */
async function signIn() {
  await FirebaseProxy.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)

  try {
    return FirebaseProxy.auth().signInWithPopup(provider)
  } catch (e) {
    throw e
  }
}

function signOut() {
  FirebaseProxy.auth().signOut().catch(console.error)
}

FirebaseProxy.auth().onAuthStateChanged(emitAuthState)

function* emitAuthState(currentUser) {
  yield currentUser
}

function test() {
  store.dispatch(UserEvents.testTimeOut)
}

export default { signIn, signOut, test }
