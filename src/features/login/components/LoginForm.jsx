import React from 'react'
import { connect } from 'react-redux'
import { onLoggedIn } from '../authReducer'

const mapStateToProps = state => {
  return { uid: state.auth.uid }
}

const mapDispatchToProps = dispatch => {
  return { onLoggedIn }
}

function LoginForm({ uid, onLoggedIn }) {
  return (<>
    uid: {uid}
    <div>
      <input type="button" value={'Login'} onClick={() => onLoggedIn('uid')}/>
    </div>
  </>)
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm)
