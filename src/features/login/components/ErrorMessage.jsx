import React from 'react'
import { connect } from 'react-redux'

function mapStateToProps(state) {
  return {
    message: state.auth.error,
  }
}

function ErrorMessage({ message }) {
  return (<>{message}</>)
}

export default connect(mapStateToProps)(ErrorMessage)
