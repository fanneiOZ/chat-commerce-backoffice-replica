import React from 'react'
import { Redirect, Route as ReactRoute } from 'react-router-dom'

export default function Route({ requireLogin = false, children, ...rest }) {
  if (requireLogin) {
    return (<Redirect to="/login"/>)
  }

  return (<ReactRoute {...rest}>{children}</ReactRoute>)
}
