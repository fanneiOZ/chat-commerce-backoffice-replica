import React from 'react'
import PropTypes from 'prop-types'
import logoAllChat from 'images/logo-all-chat.webp'

export default function AppCover({ layout }) {
  return (
    <>
      <img src={logoAllChat} />
      <div>ALL Chat Admin Panel</div>
    </>
  )
}

AppCover.propTypes = {
  layout: PropTypes.string,
}
