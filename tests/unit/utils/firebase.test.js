import {jest} from '@jest/globals'
import FirebaseProxy from '../../../src/utils/firebase'

describe('Firebase Proxy', () => {
  afterEach(() => {
    jest.resetModules()
  })
  describe('getApp', () => {
    it('Should return default app', () => {
      const app = FirebaseProxy.app()

      expect(app.name).toBe('[DEFAULT]')
    })
  })

  describe('', function () {

  })
})
