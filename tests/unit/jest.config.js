export default {
  transform: {},
  automock: false,

  clearMocks: true,

  collectCoverage: true,

  // The directory where Jest should output its coverage files
  coverageDirectory: 'coverage',

  coveragePathIgnorePatterns: [ '/node_modules/' ],

  // Indicates which provider should be used to instrument code for coverage
  coverageProvider: 'v8',

  coverageReporters: [ 'json' ],

  // An array of file extensions your modules use
  moduleFileExtensions: [ 'js', 'json', 'jsx', 'ts', 'tsx', 'node' ],

  testEnvironment: 'jest-environment-node',

  // The glob patterns Jest uses to detect test files
  testMatch: [ '**/tests/unit/**/*.[jt]s?(x)', '**/?(*.)+(spec|test).[tj]s?(x)' ],

  testPathIgnorePatterns: [ '/node_modules/', 'jest.config.js' ],
}
